<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListElements extends Model
{
public $table='List';
public $fillable=[
    'element',
    'type',
];

}
