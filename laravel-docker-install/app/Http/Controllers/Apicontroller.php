<?php
/**
 * Created by PhpStorm.
 * User: opilane
 * Date: 05.04.2018
 * Time: 18:09
 */

namespace App\Http\Controllers;

    use GuzzleHttp\Client;
use Illuminate\http\Request;
use App\ListElements;

class Apicontroller extends Controller
{

    public function index()
    {
        $guzzle = new Client();
        $response = $guzzle->get('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1');
        $json = $response->getBody()->getContents();
        $cards = json_decode($json);

        $deckName = $cards->deck_id;
//        var_dump($deckName);
        $guzzlecard = new Client();
        $cardResponse = $guzzlecard->get('https://deckofcardsapi.com/api/deck/'.$deckName.'/draw/?count=1');
        $jsoncard = $cardResponse->getBody()->getContents();
        $card = json_decode($jsoncard);

        $currnetCard=$card->cards[0]->code;
        //var_dump($currnetCard);

//        echo("ITS LIVE");
        return view('index');
    }
    public function getList(){
        $list = ListElements::all() ->pluck('elements','id') ;
        return $list;
    }
    public function addToList(Request $request){
        $getParams = $request->all();
        $element = new ListElement($getParams);
        $element->save();
        var_dump($element);
    }
    public function removeFromList(Request $request){
        $id = $request->get('element');
        $element = ListElement::find($id)->get();
        $element->delete();

    }
}