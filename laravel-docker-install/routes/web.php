<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Apicontroller@index');
Route::get('/list','Apicontroller@getList');
Route::get('/add','Apicontroller@getList');
Route::get('/delete/[id]','Apicontroller@removeFromList');
Route::post('/delete/[id]','Apicontroller@removeFromList');
Route::get('/chat', function (){
    return view('chat');
});